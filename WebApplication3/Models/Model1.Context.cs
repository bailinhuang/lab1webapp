﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication3.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ECCI_IS_LAB01Entities : DbContext
    {
        public ECCI_IS_LAB01Entities()
            : base("name=ECCI_IS_LAB01Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Curso> Cursoes { get; set; }
        public virtual DbSet<Estudiante> Estudiantes { get; set; }
        public virtual DbSet<Matricula> Matriculas { get; set; }
        public virtual DbSet<VistaMatricula> VistaMatriculas { get; set; }
        public virtual DbSet<Departamento> Departamentoes { get; set; }
    
        public virtual int Departameto_Insert(string nombre, Nullable<double> presupuesto)
        {
            var nombreParameter = nombre != null ?
                new ObjectParameter("Nombre", nombre) :
                new ObjectParameter("Nombre", typeof(string));
    
            var presupuestoParameter = presupuesto.HasValue ?
                new ObjectParameter("Presupuesto", presupuesto) :
                new ObjectParameter("Presupuesto", typeof(double));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Departameto_Insert", nombreParameter, presupuestoParameter);
        }
    }
}
